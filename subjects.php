<!DOCTYPE html>
<html lang="ru">
<head>
    <link rel="stylesheet" href="assets/css/style.css">
    <meta charset="utf-8">
    <title>Сторінка</title>
</head>
<body>
<header>
    <?php include ("menu.php");?>
</header>
<div class="tablediv">
    <fieldset>
        <legend>Навчальний план</legend>
            <table>
                <tr>
                    <td><b>Предмети:</b></td>
                    <td>ОВП</td>
                    <td>Право</td>
                    <td>ТАК</td>
                    <td>СПІМ</td>
                    <td>ЕТМТ</td>
                    <td>ЧММ на ЕОМ</td>
                    <td>АТП</td>
                </tr>
                <tr>
                    <td><b>Години:</b></td>
                    <td>30</td>
                    <td>20</td>
                    <td>100</td>
                    <td>100</td>
                    <td>90</td>
                    <td>70</td>
                    <td>90</td>
                </tr>
                <tr>
                    <td><b>Лекційні години:</b></td>
                    <td>30</td>
                    <td>20</td>
                    <td>100</td>
                    <td>100</td>
                    <td>90</td>
                    <td>70</td>
                    <td>90</td>
                </tr>
                <tr>
                    <td><b>Практичні години:</b></td>
                    <td>30</td>
                    <td>20</td>
                    <td>100</td>
                    <td>100</td>
                    <td>90</td>
                    <td>70</td>
                    <td>90</td>
                </tr>
                <tr>
                    <td><b>Лабораторні години:</b></td>
                    <td>30</td>
                    <td>20</td>
                    <td>100</td>
                    <td>100</td>
                    <td>90</td>
                    <td>70</td>
                    <td>90</td>
                </tr>
                <tr>
                    <td><b>Кредити:</b></td>
                    <td>30</td>
                    <td>20</td>
                    <td>100</td>
                    <td>100</td>
                    <td>90</td>
                    <td>70</td>
                    <td>90</td>
                </tr>
            </table>
    </fieldset>
</div>
</body>
</html>